# CONTRIBUTING.md

### Настройка VS code

Use "http.proxyStrictSSL": false in your settings.json file.

Setup extensions
```sh
$ code --install-extension ms-python.python
$ code --install-extension njpwerner.autodocstring
$ code --install-extension ms-toolsai.jupyter
$ code --install-extension charliermarsh.ruff
$ code --install-extension eamodio.gitlens
$ code --install-extension elagil.pre-commit-helper
$ code --install-extension wk-j.save-and-run
$ code --install-extension mde.select-highlight-minimap
$ code --install-extension sourcery.sourcery
$ code --install-extension ms-python.mypy-type-checker
$ code --install-extension ms-python.vscode-pylance
```

### Файл Settings.json:
```sh
{
    "markdown.preview.typographer": true,
    "markdown.extension.orderedList.marker": "one",

    "editor.wordBasedSuggestions": "off",
    "editor.fontLigatures": true,
    "editor.renderWhitespace": "all",
    "editor.rulers": [
        120
    ],

    "emmet.triggerExpansionOnTab": true,
    "emmet.includeLanguages": {
        "plaintext": "html"
    },

    "saveAndRun": {
        "commands": [{
                "match": ".py",
                "cmd": "ruff check --config=${workspaceFolder}/pyproject.toml ${file}",
                "silent": true,
            },
            {
                "match": ".py",
                "cmd": "ruff format --config=${workspaceFolder}/pyproject.toml ${file}",
                "silent": true,
            },
        ]
    }
}
```

### pre-commit
https://pre-commit.com/

pre-commit install
